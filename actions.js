// head column event listener
[...document.getElementById('head-column').children].forEach((product, productIndex) => {
	product.addEventListener("click", () => addOrder(productIndex));
});

// tracked
var orderTable = document.getElementById("order-table");


// restore url state
if (window.location.hash.length > 1) {
	orders = window.location.hash.slice(1).split('_')
	//window.location.hash = "#"
	console.log("restore orders:", orders)
	orders.forEach(order => {
		addOrder(order.split('-')[1], order.split('-')[0])
	})
}


function addOrder(productIndex, name = undefined) {
	// show
	var newcolumn = document.getElementById("column-template").firstElementChild.cloneNode(true);
	newcolumn.id = name ? name : 'o' + Date.now();
	console.log("addOrder", newcolumn.id);
	newcolumn.children[productIndex].classList.remove('void');
	newcolumn.children[productIndex].addEventListener("click", () => removeOrder(newcolumn.id));
	orderTable.appendChild(newcolumn);

	// counter
	var counter = document.getElementById("head-column").children[productIndex].firstElementChild
	var count = parseInt(counter.innerHTML)
	if (count == 0) {
		counter.classList.remove("counter-zero")
	}
	counter.innerHTML = count + 1
	
	// url hash
	if (name == undefined) {
		var hash = window.location.hash
		if (hash.length == 0) {
			hash += "#"
		} else {
			hash += "_"
		}
		hash += newcolumn.id + "-" + productIndex
		window.location.hash = hash
	}

	markBestNextChoice();
	if (!name) toast(productIndex, true);
}

function removeOrder(orderID) {
	console.log("removeOrder", orderID);
	orderTable.querySelector('#' + orderID).remove();

	// url hash
	var productIndex = -1
	var hash_items = window.location.hash
		.slice(1)
		.split('_')
		.filter(e => {
			data = e.split('-')
			if (data[0] == orderID) productIndex = data[1]
			return data[0] != orderID
		})
	window.location.hash = "#" + hash_items.join('_')
	
	// counter
	if (productIndex != -1) {
		var counter = document.getElementById("head-column").children[productIndex].firstElementChild
		var count = parseInt(counter.innerHTML)
		if (count == 1) {
			counter.classList.add("counter-zero")
		}
		counter.innerHTML = count - 1
	}

	markBestNextChoice();
	toast(productIndex, false);
}

function markBestNextChoice() {
	orderTable.querySelectorAll('.item').forEach(item => {
		item.classList.remove('marked')
	});

	var bestOrderIndexesList  = [];
	var bestOrdersValue = 5;

	var combinedOrderIndex = undefined;
	var combinedValue = 0;

	for (var t = 1; t <= Math.min(3, orderTable.children.length - 1); t++) {
		[...orderTable.children[t].children].forEach((order, productIndex) => {
			if (order.classList.contains('void') || order.classList.contains('meta')) {
				return;
			}

			const timePenalty = t - 1;
			const orderIndex = [t, productIndex];
			var orderIndexesList  = [];
			var ordersValue = 0;
			if (orderTable.children[0].children[productIndex].classList.contains('combinable')) {
				if (combinedOrderIndex == undefined) {
					combinedOrderIndex = orderIndex;
					combinedValue = 1 + timePenalty;
					orderIndexesList = [orderIndex];
					ordersValue = 4 + timePenalty;
				} else {
					orderIndexesList  = [combinedOrderIndex, orderIndex]
					ordersValue = combinedValue + 1;
					combinedOrderIndex = undefined;
					combinedValue = 0;
				}
			} else {
				orderIndexesList  = [orderIndex];
				ordersValue = 2 + timePenalty;
			}

			if (ordersValue < bestOrdersValue) {
				bestOrderIndexesList  = orderIndexesList ;
				bestOrdersValue = ordersValue;
			}
		});
	}

	bestOrderIndexesList.forEach(order => {
		const t = order[0];
		const productIndex = order[1];
		orderTable.children[t].children[productIndex].classList.add('marked');
	});
}

function toast(productIndex, encued) {
	// get product name
	retrieveString = e => (e.innerText || e.textContent).split('\n')[0]
	var productName = retrieveString(orderTable.children[0].children[productIndex]);

	var newtoast = document.getElementById("toast-template").firstElementChild.cloneNode(true);
	if (encued) {
		newtoast.classList.add('encue');
		newtoast.innerHTML = "Encued 1 " + productName
	} else {
		newtoast.classList.add('decue');
		newtoast.innerHTML = "Decued 1 " + productName
	}

	document.getElementById("toasts").prepend(newtoast);
	setTimeout(() => newtoast.remove(), 4000)
}

function zoom(zoomIn) {
	if(zoomIn) document.getElementById("style-condensed").disabled = false
	else document.getElementById("style-condensed").disabled = true
}
document.getElementById("zoom-in").addEventListener("click", () => zoom(true))
document.getElementById("zoom-out").addEventListener("click", () => zoom(false))