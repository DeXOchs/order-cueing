# Order Cueing

This web project describes a single-page web application where possible orders (here: at a café, so its espresso, cappucchino etc.) can be cued in and out by touch actions.

- Orders can be cued in by clicking on the order name (the leftmost column). The order is cued in to the right, so the least recent order is always the leftmost.
- Orders can be cued out by clicking on the order box itself.

The app also implements a proposal algorithm which highlights orders best fit beeing processed together. For example can two single espressi be processed better by dividing a double espresso shot into two cups. So these two espressi probably should be done together before other orders.
Currently the algorithm is a single-dimension optimizer which penalizes uncombinable and most recent orders over least recent combinable orders. One can easily implement other proposal/ optimization algorithms, e.g. penalizing bulk orders all the same or use penalty matrices for order combinations.
